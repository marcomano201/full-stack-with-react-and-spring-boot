# full-stack-with-react-and-spring-boot

## Name
Full-stack with react and spring boot

## Description
I am following this course "Build Your First Java Full Stack Application with React & Spring Boot. Become a Java Full Stack Java Web Developer Now!". Here I will post the code and some notes about the course/code to get even more knowledge 

## Why build a full-stack application
IDK Yet

## Create a react app

*To create a React Application run the following command:*

- **npx create-react-app "nameProject"**
        
        Creates a React application

*This process will take a few minutes, once is finished navigate to the projects folder and launch the app:*

- **cd "nameProject"**

        Navigate to the projects folder

- **npm start**

        Starts the development server.

- **npm run build**

        Bundles the app into static files for production.

- **npm test**

        Starts the test runner.

- **npm run eject**

        Removes this tool and copies build dependencies, configuration files, scripts into the app directory. If you do this, you can’t go back!

React app is built as a group of Components itch of this Components is independent and reusable on its own
        
***Component:***

- View:

        JSX or Javascript
        
- Logic:

        Javascript

- Styling:

        CSS

- Static:

        Internal Data Store
        
- Props:

        Pass Data